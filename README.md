# mastersync-remote-admin
keep linux servers in sync... an easy open solution for linux administrators

Found on linked in, all started with this question

"How to run remote command on a lot of servers at the same time

Dears , How to run a same remote command on 1000 servers or more at once and store the output on one file on your local machine without using 3rd party APP. ?

if we are using "ssh" in "for" loop, it will take a lot of time, is there any other way to run the remote command on all servers at the same time ?"
Thank you Ahmed Gamil ( https://www.linkedin.com/profile/view?id=ADcAAAxrJrsB06C7UxpXAb_5z6dM9ciwXhBpwms&authType=name&authToken=EHq0 ) for asking.

From that, an ideea was arised... what if we can use existing powerfull tools in order to perform such a task ?
What is the best way to scale ?
Our idee is that every server can act as clients for a central server that send message to 

Remote Servers can 

1. be listed individual
2. be part of groups
3. notify about conditions the central admin panel
4. upgraded, etc.

No need to ssh into, just old plain https based messaging - but also a realtime and lowlatency operation can be imagined.

Why not let a station be also managed ? We see no reason not to do that.

This is the main part. But also, you can control a pice of software running on the remote server in reverse: there is no reason not to be able to request informations about a server or to force performing such an operation per server,server group.
And the posibilities are limitless.

So here we go...

## How it works:

### 1. Local site sync manager (site means one data center with direct network connection between servers. in a location it can be 2 ore more sites separated or not. )
We have a MasterSyncServer that perform message sync operations and remote nodes 
each remote node run syncClient component. The client perform -> register operation (register itself as a station ) -> it send current time, next time to check(interval) and station id to the central server.
If a client does not perform aliveconfirm operation in specified interval of time, the coresponding entry is marked as down on the central server. It receive a confirmation message

In order to have redundancy, the client can register two or more endpoints of mastersync server to connect to in order to receive sync messages

After registration, on aliveconfirmation operation, it receive messages available (maybe on all operation? ) as stated above


Another server can see connected stations list on the server(or redundant server farm ).
Goals:
-lightweight messages
-independent run
-redundancy


### 2. Inter site sync manager 

The system can be used to sync tasks (or send any other message ) between central servers of the site. It is easy because we have dns model of server intercomunication

Having MasterSync.1...MasterSync.n 
each server is configured to serve a site (like a dns zone ) and actually use dns-like system to perform operations







